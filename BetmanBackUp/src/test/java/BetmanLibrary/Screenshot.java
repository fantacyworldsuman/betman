package BetmanLibrary;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class Screenshot {

	WebDriver dr;
	public static void screenshotCapture(WebDriver dr, String screenshotName)
	{
		try {
			TakesScreenshot ts = (TakesScreenshot)dr;
			File source = ts.getScreenshotAs(OutputType.FILE);
			String dest = "./Screenshots/" + screenshotName + ".png";
			File destination = new File(dest);
			FileUtils.copyFile(source, destination);
		} catch (Exception e) {
			System.out.println("The Exception is : " + e.getMessage());
		}
	}
}
