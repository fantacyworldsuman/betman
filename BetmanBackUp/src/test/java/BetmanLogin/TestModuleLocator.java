package BetmanLogin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class TestModuleLocator {

	
	@FindBy(how=How.XPATH, using="//a[@class='md-button md-ink-ripple' and @aria-label='Logout']") 
	WebElement logoutButton;
	
	public void settingsClick() throws Exception
	{
		Thread.sleep(2000);
		logoutButton.click();		
	}
}

