package BetmanLogin;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import BetmanLibrary.DataDriven;
import BetmanLibrary.Screenshot;

public class LoginBase {

	static WebDriver dr;
	String browsernm;
	Scanner scanner;
	
	@BeforeClass
	@Parameters("browser")
	public void browserSelection(String browserName)
	{
		this.browsernm=browserName;
	}
		
	public WebDriver browserOpen()
	{		
	 if(browsernm.equalsIgnoreCase("chrome"))
		{
			System.setProperty("webdriver.chrome.driver", "./Drivers/Chrome_Driver/chromedriver.exe");
			dr = new ChromeDriver();
			System.out.println("Please enter the URL of the application : ");
		}
		else if(browsernm.equalsIgnoreCase("firefox"))
		{
			System.setProperty("webdriver.gecko.driver", "./Drivers/Firefox_Driver/geckodriver.exe");
			dr = new FirefoxDriver();
			System.out.println("Please enter the URL of the application : ");
		}

	 
	 	scanner = new Scanner(System.in);
	 	String pageURL = scanner.next();
		dr.get(pageURL);
		dr.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
		//dr.manage().window().maximize();
		return dr;
	}	

	
	@AfterMethod
	public void tcChecking(ITestResult result)
	{
		if(result.getStatus()==ITestResult.FAILURE)
		{
			Screenshot.screenshotCapture(dr, result.getName());
		}
	}
	
	@AfterClass
	public void browserQuit()
	{
		dr.quit();
	}
}

