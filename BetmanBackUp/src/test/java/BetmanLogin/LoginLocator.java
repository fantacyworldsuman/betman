package BetmanLogin;

import java.util.Scanner;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

public class LoginLocator {

protected WebDriver driver;
Alert alert;
	
	@FindBy(how=How.ID, using="input_0") 
	WebElement usernameField;
	@FindBy(how=How.ID, using="input_1") 
	WebElement passwordField;
	@FindBy(how=How.XPATH, using="//button[@class='md-primary md-raised md-button md-ink-ripple']") 
	WebElement loginButton;
	
	@FindBy(how=How.XPATH, using="//p[@class='error-message']")
	WebElement loginFailedText;
	
	@FindBy(how=How.XPATH, using="//md-icon[@class='material-icons ng-scope']")
	WebElement logoutButton;
	
	
	String expectedTitle = "Betman";
	String expectedErrorMessage1 = "Login failed";
	String expectedErrorMessage2 = "Your username could not be found in our Database";
	
	
	public LoginLocator(WebDriver dr)
	{
		this.driver=dr;
	}

	
	public void credentialVisibility() throws Exception
	{
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the username : ");
		String usernm = scanner.next();
		usernameField.sendKeys(usernm);;
		Boolean usernameVisibility = usernameField.isDisplayed();
		if(usernameVisibility == true)
		{
			System.out.println("The Username is visible and it appears within the Username textfield." + "\n");
		}
		else
			System.out.println("The Username is not visible." + "\n");
		 
		System.out.println("Enter the password : ");
		String passwrd = scanner.next();
		passwordField.sendKeys(passwrd);		
		boolean passwordEncryption = passwordField.getAttribute("type").equals("password");
		if(passwordEncryption == true)
		{
			if(passwordField.isDisplayed())
				System.out.println("The Password is encrypted and it appears within the Password textfield." + "\n");
			else
				System.out.println("The Password is encrypted but its not visible within the Password textfield" + "\n");
		}
		else
		System.out.println("The Password isn't encrypted.");
	}
	
	
	public void successfulLogin() throws Exception
	{
		loginButton.click();
		String actualTitle = driver.getTitle();
		if(logoutButton.isDisplayed()&&(actualTitle.equalsIgnoreCase(expectedTitle)))
		{
			System.out.println("Logout button appearing and the title is also matching which is : '" + actualTitle + "'. So the user has successfully logged into the application." + "\n");	
		}
		else
			System.out.println("User is unable to log into the application" + "\n");		
	}	
	
	
	public void browserBackClick() throws Exception
	{
		driver.navigate().back();	Thread.sleep(3000);
		String currentTitle = driver.getTitle();
		Assert.assertEquals(currentTitle, expectedTitle, "Title is not matching so the user has gone back to the Login page and the current title is : " + currentTitle + "\n");
		System.out.println("User is still in logged in mode after clicking Browser Back button, so the test case has been passed." + "\n");
	}
	
	
	public void usingInvalidCredentials(String username, String password)
	{
		usernameField.sendKeys(username);
		passwordField.sendKeys(password);
		if(loginButton.isEnabled())
		{
			loginButton.click();
			String actualErrorMessage = loginFailedText.getText();
			if(actualErrorMessage.equalsIgnoreCase((expectedErrorMessage1))||(actualErrorMessage.equalsIgnoreCase(expectedErrorMessage2)))
			{
				System.out.println("The user has not been able to log into the application using the invalid credential, so the Test Case has been passed.");
			}
			else
			{
				System.out.println("The Test Case has been failed.");
			}
		}
		else
			System.out.println("The Login button is disabled. Please insert valid username or password");

	}
	
	
}
