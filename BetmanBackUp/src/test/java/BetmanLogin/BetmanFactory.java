package BetmanLogin;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import BetmanLibrary.DataDriven;
import BetmanLogin.LoginLocator;


public class BetmanFactory extends LoginBase{
	
	WebDriver dr;
	
	DataDriven d_fetch = null;
	String xlFilePath = "./DataFetch/Login_Data.xlsx";
	String sheetName = "Credentials";
	LoginLocator log;
	
	@Test(priority=0)
	public void userLogin() throws Exception
	{
		dr=browserOpen();
		log = PageFactory.initElements(dr, LoginLocator.class);
		log.credentialVisibility();
		log.successfulLogin();
		log.browserBackClick();
	}
	
	/*
	@Test(priority=1, dataProvider = "userData")
	public void fillUserForm(String userName, String password)
	{
		browserOpen();
		log = PageFactory.initElements(dr, LoginLocator.class);
		log.usingInvalidCredentials(userName, password);
	}
	
	
	@DataProvider(name="userData")
	public Object[][] userFormData() throws Exception
	{
		Object[][] data = testData(xlFilePath, sheetName);
		return data;
	}
	
	
	public Object[][] testData(String xlFilePath, String sheetName) throws Exception
	{
		Object[][] excelData = null;
		d_fetch = new DataDriven(xlFilePath);
		
		int rows = d_fetch.getRowCount(sheetName);
		int columns = d_fetch.getColumnCount(sheetName);
		
		excelData = new Object[rows-1][columns];
		
		for(int i=1; i<rows; i++)
		{
			for(int j=0; j<columns; j++)
			{
				excelData[i-1][j] = d_fetch.getCellData(sheetName, j, i);
			}
		}
		return excelData;
	}	*/
}

